part of 'data_bloc.dart';

abstract class DataState extends Equatable {
  const DataState();

  @override
  List<Object> get props => [];
}

class DataInitial extends DataState {}

class DataReceived extends DataState {
  final List<Model> data;

  const DataReceived(this.data);

  @override
  List<Object> get props => [data];
}
