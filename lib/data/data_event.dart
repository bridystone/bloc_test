part of 'data_bloc.dart';

abstract class DataEvent extends Equatable {
  const DataEvent();

  @override
  List<Object> get props => [];
}

class DataRequest extends DataEvent {
  final int requestId;
  const DataRequest(this.requestId);

  @override
  List<Object> get props => [requestId];
}
