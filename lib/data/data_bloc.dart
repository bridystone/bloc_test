import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:bloc_test/model.dart';
import 'package:equatable/equatable.dart';

part 'data_event.dart';
part 'data_state.dart';

class DataBloc extends Bloc<DataEvent, DataState> {
  DataBloc() : super(DataInitial()) {
    on<DataRequest>(_onDataRequest);
  }

  void _onDataRequest(DataRequest event, Emit<DataState> emit) {
    var modelData = List<Model>.generate(
        event.requestId,
        (index) =>
            Model(Random().nextInt(event.requestId), "BLABLABLA $index"));
    emit(DataReceived(modelData));
  }
}
