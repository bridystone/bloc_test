import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:bloc_test/model.dart';
import 'package:equatable/equatable.dart';

part 'sub_data_event.dart';
part 'sub_data_state.dart';

class SubDataBloc extends Bloc<SubDataEvent, SubDataState> {
  SubDataBloc() : super(SubDataInitial()) {
    on<SubDataRequest>(_onSubDataRequest);
  }

  void _onSubDataRequest(SubDataRequest event, Emit<SubDataState> emit) async {
    final percent = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    for (var i in percent) {
      emit(SubDataUpdating(i * 10));
      await Future<void>.delayed(Duration(seconds: 1));
    }
    emit(SubDataUpdating(100));

    var modelData = List<SubModel>.generate(
        event.requestId,
        (index) =>
            SubModel(Random().nextInt(event.requestId), "BLABLABLA $index"));
    emit(SubDataReceived(modelData));
  }
}
