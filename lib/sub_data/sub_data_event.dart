part of 'sub_data_bloc.dart';

abstract class SubDataEvent extends Equatable {
  const SubDataEvent();

  @override
  List<Object> get props => [];
}

class SubDataRequest extends SubDataEvent {
  final int requestId;
  const SubDataRequest(this.requestId);

  @override
  List<Object> get props => [requestId];
}
