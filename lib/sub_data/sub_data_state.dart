part of 'sub_data_bloc.dart';

abstract class SubDataState extends Equatable {
  const SubDataState();

  @override
  List<Object> get props => [];
}

class SubDataInitial extends SubDataState {}

class SubDataUpdating extends SubDataState {
  final int percent;
  const SubDataUpdating(this.percent);

  @override
  List<Object> get props => [percent];
}

class SubDataReceived extends SubDataState {
  final List<SubModel> subdata;

  const SubDataReceived(this.subdata);

  @override
  List<Object> get props => [subdata];
}
