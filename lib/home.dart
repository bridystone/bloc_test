import 'package:bloc_test/data/data_bloc.dart';
import 'package:bloc_test/model.dart';
import 'package:bloc_test/sub_data/sub_data_bloc.dart';
import 'package:bloc_test/view/view_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<DataBloc>(context).add(DataRequest(20));
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            Text('TEST'),
            IconButton(
              onPressed: () {
                final viewBloc = BlocProvider.of<ViewBloc>(context);
                viewBloc.add(ViewSorting(
                    viewBloc.currentOrder == Sortorder.ascending
                        ? Sortorder.descending
                        : Sortorder.ascending));
              },
              icon: Icon(Icons.sort),
            )
          ],
        ),
      ),
      body: BlocBuilder<ViewBloc, ViewState>(
        builder: (context, state) {
          if (state is ViewInitial) {
            return CircularProgressIndicator();
          } else if (state is ViewReadyForUI) {
            return ListView.builder(
              itemCount: state.sortedData.length,
              itemBuilder: (context, index) => BlocProvider(
                create: (context) => SubDataBloc(),
                child: MyTile(
                  dataItem: state.sortedData[index],
                ),
              ),
            );
          }
          return Text('should not happen');
        },
      ),
    );
  }
}

class MyTile extends StatelessWidget {
  final Model dataItem;
  const MyTile({
    Key? key,
    required this.dataItem,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //BlocProvider.of<SubDataBloc>(context).add(SubDataRequest(dataItem.id));
    return BlocBuilder<SubDataBloc, SubDataState>(
      builder: (context, state) {
        return ListTile(
          leading: Text(dataItem.id.toString()),
          title: Text(dataItem.text),
          trailing: (state is SubDataReceived)
              ? Text('items: ${state.subdata.length}')
              : (state is SubDataUpdating)
                  ? Text('${state.percent}')
                  : Text('initial'),
          onTap: () => BlocProvider.of<SubDataBloc>(context)
              .add(SubDataRequest(dataItem.id)),
        );
      },
    );
  }
}
