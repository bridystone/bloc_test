import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
class Model extends Equatable {
  final String text;
  final int id;
  const Model(this.id, this.text);

  @override
  List<Object?> get props => [id, text];
}

@immutable
class SubModel extends Equatable {
  final String text;
  final int id;
  const SubModel(this.id, this.text);

  @override
  List<Object?> get props => [id, text];
}
