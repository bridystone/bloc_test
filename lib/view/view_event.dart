part of 'view_bloc.dart';

abstract class ViewEvent extends Equatable {
  const ViewEvent();

  @override
  List<Object> get props => [];
}

enum Sortorder { unsorted, ascending, descending }

class ViewSorting extends ViewEvent {
  final Sortorder order;
  const ViewSorting(this.order);

  @override
  List<Object> get props => [order];
}
