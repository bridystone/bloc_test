part of 'view_bloc.dart';

abstract class ViewState extends Equatable {
  const ViewState();

  @override
  List<Object> get props => [];
}

class ViewInitial extends ViewState {}

class ViewReadyForUI extends ViewState {
  final List<Model> sortedData;
  final Sortorder currentSort;
  const ViewReadyForUI(this.sortedData, this.currentSort);

  @override
  List<Object> get props => [sortedData];
}
