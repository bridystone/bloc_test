import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bloc_test/data/data_bloc.dart';
import 'package:bloc_test/model.dart';
import 'package:equatable/equatable.dart';

part 'view_event.dart';
part 'view_state.dart';

class ViewBloc extends Bloc<ViewEvent, ViewState> {
  DataBloc dataBloc;
  late StreamSubscription streamSubscription;

  ViewBloc(this.dataBloc) : super(ViewInitial()) {
    streamSubscription = dataBloc.stream.listen((state) {
      if (state is DataReceived) {
        add(ViewSorting(Sortorder.unsorted));
      }
    });
    on<ViewSorting>(_onViewSorting);
  }

  void _onViewSorting(ViewSorting event, Emit<ViewState> emit) {
    if (dataBloc.state is DataReceived) {
      var data = List<Model>.from((dataBloc.state as DataReceived).data);
      switch (event.order) {
        case Sortorder.ascending:
          data.sort((a, b) => a.id - b.id);
          break;
        case Sortorder.descending:
          data.sort((a, b) => b.id - a.id);
          break;
        default:
      }
      emit(ViewReadyForUI(data, event.order));
    }
  }

  @override
  Future<void> close() {
    streamSubscription.cancel();
    return super.close();
  }

  Sortorder get currentOrder => (state is ViewReadyForUI)
      ? (state as ViewReadyForUI).currentSort
      : Sortorder.unsorted;
}
